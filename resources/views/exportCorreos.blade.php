<h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
<br>
<h3>Total de ordenes realizadas por usuarios</h3>
<br>
<table style=" border-collapse: collapse;">
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'>Usuario</th>
            <th style='text-align:center; font-weight: bold;'>Ordenes realizadas</th>
         </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)  
            <tr>
                <td style='text-align:center;'>{{$usuario['usuario']}}</td>
                <td style='text-align:center;'>{{$usuario['ordenes']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<br>
<br>
<h3>Ordenes realizadas</h3>
<br>
<table>
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'>Solicitud/Orden</th>
            <th style='text-align:center; font-weight: bold;'>Paciente</th>
            <th style='text-align:center; font-weight: bold;'>Cédula</th>
            <th style='text-align:center; font-weight: bold;'>Teléfonos</th>
            <th style='text-align:center; font-weight: bold;'>Email</th> 
            <th style='text-align:center; font-weight: bold;'>Creado por</th> 
            <th style='text-align:center; font-weight: bold;'>Técnico</th> 
            <th style='text-align:center; font-weight: bold;'>Radiólogo</th> 
            <th style='text-align:center; font-weight: bold;'>Transcriptor</th> 
            <th style='text-align:center; font-weight: bold;'>Aprobado por</th> 
            <th style='text-align:center; font-weight: bold;'>Culminada</th> 
         </tr>
    </thead>
    <tbody>
        @foreach($result as $info)  
            <tr>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->id}} / {{$info->orden}} ({{$info->description}})</td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->patient_first_name}} {{$info->patient_last_name}}</td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->patient_ID}}</td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->telephone_number}} / {{$info->cellphone_number}} </td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->email}}</td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->first_name}} {{$info->last_name}}</td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>
                    @if($info->technician_user_name)
                        {{$info->technician_user_name}} ({{$info->technician_end_date}})
                    @endif
                </td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>
                    @if($info->radiologist_user_name)
                        {{$info->radiologist_user_name}} ({{$info->dictation_date}})
                    @endif
                </td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>
                    @if($info->transcriptor_user_name)
                        {{$info->transcriptor_user_name}} ({{$info->transcription_date}})
                    @endif
                </td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>
                    @if($info->approve_user_name)
                        {{$info->approve_user_name}} ({{$info->approval_date}})
                    @endif
                </td>
                <td style='text-align:center; font-size: 12px; padding: 5px;'>
                    @if($info->culmination_date)
                        {{$info->culmination_date}}
                    @endif
                </td>
            </tr>
       @endforeach
    </tbody>
</table>