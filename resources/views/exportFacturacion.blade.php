<h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
<br>
<br>
<table>
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'># Orden</th>
            <th style='text-align:center; font-weight: bold;'>Cédula</th>
            <th style='text-align:center; font-weight: bold;'>Procedimiento/Estudio</th>
            <th style='text-align:center; font-weight: bold;'>Estatus</th>
            <th style='text-align:center; font-weight: bold;'>Viewmed</th>
            <th style='text-align:center; font-weight: bold;'>Informe Viewmed</th>
            <th style='text-align:center; font-weight: bold;'>Img Viewmed</th>
            <th style='text-align:center; font-weight: bold;'>Img Pacs</th>
            <th style='text-align:center; font-weight: bold;'>Diferencia</th>
            @if($fact == 1)
                <th style='text-align:center; font-weight: bold;'>Facturación</th>
            @endif
            <th style='text-align:center; font-weight: bold;'>Fecha creado</th>
            <th style='text-align:center; font-weight: bold;'>Fecha culminado</th>
            <th style='text-align:center; font-weight: bold;'>Observación</th>
            <th style='text-align:center; font-weight: bold;'>Paciente</th>
            <th style='text-align:center; font-weight: bold;'>Teléfonos</th>
            <th style='text-align:center; font-weight: bold;'>Email</th>
            <th style='text-align:center; font-weight: bold;'>Creado por</th> 
            <th style='text-align:center; font-weight: bold;'>Técnico</th> 
            <th style='text-align:center; font-weight: bold;'>Radiólogo</th> 
            <th style='text-align:center; font-weight: bold;'>Transcriptor</th> 
            <th style='text-align:center; font-weight: bold;'>Aprobado por</th> 
            <th style='text-align:center; font-weight: bold;'>Culminada</th>
         </tr>
    </thead>
    <tbody>
        @foreach($result as $info)
            <tr>
                <td style='text-align:center;'>{{$info->id}}</td>
                <td style='text-align:center;'>{{$info->patient_identification_id}}</td>
                <td style='text-align:center;'>{{$info->description}} ({{$info->modalidad}})</td>
                <td style='text-align:center;'>
                    {{$info->status}}
                    @if($info->suspension_reason_id != null)
                        ({{$info->suspension}})
                    @endif
                </td>
                <td style='text-align:center;'>
                    @if($info->viewmed == true)
                        SI
                    @else
                        NO
                    @endif
                </td>
                <td style='text-align:center;'>{{$info->report}}</td>
                <td style='text-align:center;'>{{$info->img_viewmed}}</td>
                <td style='text-align:center;'>{{$info->img_pacs}}</td>
                <td style='text-align:center;'>{{$info->img_viewmed - $info->img_pacs}}</td>
                @if($fact == 1)
                    <td style='text-align:center;'>
                        {{$info->nro_factura}} ({{$info->estatus_factura}})
                    </td>
                @endif
                <td style='text-align:center;'>{{$info->created_at}}</td>
                <td style='text-align:center;'>{{$info->culmination_date}}</td>
                <td style='text-align:center;'>
                    @if($info->cedula == 'N')
                        La orden no esta aun en Viewmed
                    @elseif($info->cedula == 'S')

                    @elseif($info->cedula == 'E')
                        La cédula no coincide ({{$info->cedulaViewmed}})
                    @elseif($info->cedula == 'P')
                        Orden de prueba
                    @endif
                </td>
                <td style='text-align:center;'>{{$info->patient_first_name}} {{$info->patient_last_name}}</td>
                <td style='text-align:center;'>{{$info->telephone_number}} / {{$info->cellphone_number}} </td>
                <td style='text-align:center;'>{{$info->email}}</td>
                <td style='text-align:center;'>{{$info->first_name}} {{$info->last_name}} ({{$info->created_date}})</td>
                <td style='text-align:center;'>
                    @if($info->technician_user_name)
                        {{$info->technician_user_name}} ({{$info->technician_end_date}})
                    @endif
                </td>
                <td style='text-align:center;'>
                    @if($info->radiologist_user_name)
                        {{$info->radiologist_user_name}} ({{$info->dictation_date}})
                    @endif
                </td>
                <td style='text-align:center;'>
                    @if($info->transcriptor_user_name)
                        {{$info->transcriptor_user_name}} ({{$info->transcription_date}})
                    @endif
                </td>
                <td style='text-align:center;'>
                    @if($info->approve_user_name)
                        {{$info->approve_user_name}} ({{$info->approval_date}})
                    @endif
                </td>
                <td style='text-align:center;'>
                    @if($info->culmination_date)
                        {{$info->culmination_date}}
                    @endif
                </td>
            </tr>
       @endforeach
    </tbody>
</table>