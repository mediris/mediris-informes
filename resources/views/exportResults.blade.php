<h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
<br>
<h3>Total de resultados entregados por usuarios</h3>
<br>
<table style=" border-collapse: collapse;">
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'>Usuario</th>
            <th style='text-align:center; font-weight: bold;'>N° estudios entregados</th>
         </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)  
            <tr>
                <td style='text-align:center;'>{{$usuario['usuario']}}</td>
                <td style='text-align:center;'>{{$usuario['ordenes']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<br>
<br>
<h3>Resultados entregados por usuarios</h3>
<br>
<table>
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'>Receptor</th>
            <th style='text-align:center; font-weight: bold;'>Cédula receptor</th>
            <th style='text-align:center; font-weight: bold;'>Número de orden</th>
            <th style='text-align:center; font-weight: bold;'>Fecha de entrega</th>    
            <th style='text-align:center; font-weight: bold;'>CD</th> 
            <th style='text-align:center; font-weight: bold;'>Informe</th> 
            <th style='text-align:center; font-weight: bold;'>Creado por</th>
         </tr>
    </thead>
    <tbody>
        @foreach($result as $info)  
            <tr>
                <td style='text-align:center;'>{{$info->receptor_name}}</td>
                <td style='text-align:center;'>{{$info->receptor_id}}</td>
                <td style='text-align:center;'>{{$info->requested_procedure_id}}</td>
                <td style='text-align:center;'>{{$info->date}} </td>
                <td style='text-align:center;'>
                    @if($info->cd == 1)
                        SI ({{$info->num_cd}})
                    @else
                        NO
                    @endif
                </td>
                <td style='text-align:center;'>
                    @if($info->file == 1)
                        SI
                    @else
                        NO
                    @endif
                </td>
                <td style='text-align:center;'>{{$info->responsable_name}}</td>
            </tr>
       @endforeach
    </tbody>
</table>