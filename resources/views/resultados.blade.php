<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Reporte resultados entregados por usuarios</title>
        <style>
            html, body {
                font-family: Arial, Helvetica;
            }

            body {
                margin-bottom: 1.5cm;
            }

            table {
                width: 100%;
            }
            td {
                white-space: nowrap;
            }

            td.label {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <script type="text/php">
            $size = 8;
            $y = 25;
            $x = $pdf->get_width() - 80;
            $font = $fontMetrics->get_font("sans-serif");
            $pdf->page_text($x, $y, " Página {PAGE_NUM}/{PAGE_COUNT}", $font, $size);
        </script>
        <main>
            <h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
            <h4>Resultados entregados por usuarios</h4>
            
            <table border = 1 cellspacing = 0 cellpadding = 0 style="margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>N° estudios entregados</th>
                     </tr>
                </thead>
                <tbody>
                    @foreach($usuarios as $usuario)  
                        <tr>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$usuario['usuario']}}</td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$usuario['ordenes']}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <hr>

            <table border = 1 cellspacing = 0 cellpadding = 0 style="margin-top: 10px">
                <thead>
                    <tr>
                        <th>Receptor</th>
                        <th>Cédula receptor</th>
                        <th>Número de orden</th>
                        <th>Fecha de entrega</th>    
                        <th>CD</th> 
                        <th>Informe</th> 
                        <th>Creado por</th>
                     </tr>
                </thead>
                <tbody>
                    @foreach($result as $info)  
                        <tr>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$info->receptor_name}}</td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$info->receptor_id}}</td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$info->requested_procedure_id}}</td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$info->date}} </td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>
                                @if($info->cd == 1)
                                    SI ({{$info->num_cd}})
                                @else
                                    NO
                                @endif
                            </td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>
                                @if($info->file == 1)
                                    SI
                                @else
                                    NO
                                @endif
                            </td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$info->responsable_name}}</td>
                        </tr>
                   @endforeach
                </tbody>
            </table>
        </main>
    </body>
</html>