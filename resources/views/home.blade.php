@extends('layouts.app')
<nav class="navbar navbar-expand-md navbar-light shadow-sm color-primary">
    <div class="container">
        <a class="navbar-brand text-white font-weight-bold" href="{{ url('/home') }}">
            Mediris - Informes
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto"></ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-white font-weight-bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-user"></i> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}<span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Cerrar Sesión') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

    <!-- BACKDROP -->
    <div id="backdrop" class="fondo-opaco"></div>
    <!-- BACKDROP -->

    <div class="container">

        @if(Session::has('warning'))
            <div class="alert alert-warning alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session::get('warning')}}
            </div>
        @endif

        <!-- PILLS -->
        <ul class="nav nav-pills mb-3 mb-2-personalizada" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active new-btn-primary tab-a" id="pills-informe-tab" data-toggle="pill" href="#pills-informes" role="tab" aria-controls="pills-informe" aria-selected="true">Informes</a>
            </li>
            <li class="nav-item tab-secondary" role="presentation">
                <a class="nav-link new-btn-primary tab-a tab-1" id="pills-reporte-tab" data-toggle="pill" href="#pills-reporte" role="tab" aria-controls="pills-reporte" aria-selected="false">Reporte diario</a>
            </li>
            <li class="nav-item tab-secondary" role="presentation">
                <a class="nav-link new-btn-primary tab-a" id="pills-cedula-tab" data-toggle="pill" href="#pills-cedula" role="tab" aria-controls="pills-cedula" aria-selected="false">Cambio de cédula</a>
            </li>
            <li class="nav-item tab-secondary" role="presentation">
                <a class="nav-link new-btn-primary tab-a tab-3" id="pills-resultados-tab" data-toggle="pill" href="#pills-resultados" role="tab" aria-controls="pills-resultados" aria-selected="false">Resultados entregados por usuarios</a>
            </li>
            <li class="nav-item tab-secondary" role="presentation">
                <a class="nav-link new-btn-primary tab-a tab-2" id="pills-auditoria-tab" data-toggle="pill" href="#pills-auditoria" role="tab" aria-controls="pills-auditoria" aria-selected="false">Listado de auditoria</a>
            </li>
            <li class="nav-item tab-secondary" role="presentation">
                <a class="nav-link new-btn-primary tab-a tab-4" id="pills-referentes-tab" data-toggle="pill" href="#pills-referentes" role="tab" aria-controls="pills-referentes" aria-selected="false">Reporte Referentes/Visitador</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">

            <!-- PILL INFORMES -->
            <div class="tab-pane fade show active tab-personalizada" id="pills-informes" role="tabpanel" aria-labelledby="pills-informes-tab">
                <div class="card">
                   <div class="card-body">
                        <form id="form-search" action="#">
                            <div class="form-group row">
                                <div class="col-md-2"></div>
                                <label for="orden" class="col-md-3 col-form-label">N° de Orden</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control" id="orden" name="orden">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary float-right btn-form color-primary"><i class="fas fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
            <!-- PILL INFORMES -->

            <!-- PILL REPORTE -->
            <div class="tab-pane fade tab-personalizada" id="pills-reporte" role="tabpanel" aria-labelledby="pills-reporte-tab">
                <form id="formoid" method="POST" action="{{ url('reporte') }}" role="form" class="needs-validation" novalidate>
                    {{ csrf_field() }}
                    <div class="card-header">
                        <h3 class="panel-title">Reporte ordenes en Viewmed {{$institution}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="start_date">Fecha inicio</label>
                                    <input type="date" name="start_date" id="start_date" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="end_date">Fecha fin</label>
                                    <input type="date" name="end_date" id="end_date" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="submit"  value="Generar" class="btn btn-primary new-btn-primary btn-generar">
                        </div>
                    </div>
                </form>
                <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="height: 200px;">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" id="toastP" style="max-width: 400px !important;">
                        <div class="toast-header" style="color: #000 !important; background-color: rgba(0,0,0,.03);">
                            <strong class="mr-auto">Aviso</strong>
                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            Generando el reporte, esto puede demorar varios minutos. Cuando finalice el reporte estará en "Descargas"...
                        </div>
                    </div>
                </div>
            </div>
            <!-- PILL REPORTE -->

            <!-- PILL CEDULA -->
            <div class="tab-pane fade tab-personalizada" id="pills-cedula" role="tabpanel" aria-labelledby="pills-cedula-tab">
                <div class="card">
                   <div class="card-body">
                        <form id="form-cedula" action="#">
                            <div class="form-group row">
                                <div class="col-md-2"></div>
                                <label for="orden1" class="col-md-3 col-form-label">N° de Orden</label>
                                <div class="col-md-5">
                                    <input type="number" class="form-control" id="orden1" name="orden1">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary float-right btn-form-cedula color-primary"><i class="fas fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
            <!-- PILL CEDULA -->

            <!-- PILL AUDITORIA -->
            <div class="tab-pane fade tab-personalizada" id="pills-auditoria" role="tabpanel" aria-labelledby="pills-auditoria-tab">
                <form id="formoid" method="POST" action="{{ url('reporteAuditoria') }}" role="form" class="needs-validation" novalidate>
                    {{ csrf_field() }}
                    <div class="card-header">
                        <h3 class="panel-title">Listado de auditoria {{$institution}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="start_date1">Fecha inicio</label>
                                    <input type="date" name="start_date1" id="start_date1" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="end_date1">Fecha fin</label>
                                    <input type="date" name="end_date1" id="end_date1" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="submit"  value="Generar" class="btn btn-primary new-btn-primary btn-generar">
                        </div>
                    </div>
                </form>
                <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="height: 200px;">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" id="toastP" style="max-width: 400px !important;">
                        <div class="toast-header" style="color: #000 !important; background-color: rgba(0,0,0,.03);">
                            <strong class="mr-auto">Aviso</strong>
                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            Generando el reporte, esto puede demorar varios minutos. Cuando finalice el reporte estará en "Descargas"...
                        </div>
                    </div>
                </div>
            </div>
            <!-- PILL AUDITORIA -->

            <!-- PILL RESULTADOS POR USUARIOS -->
            <div class="tab-pane fade tab-personalizada" id="pills-resultados" role="tabpanel" aria-labelledby="pills-resultados-tab">
                <form id="formoid" method="POST" action="{{ url('results') }}" role="form" class="needs-validation" novalidate>
                    {{ csrf_field() }}
                    <div class="card-header">
                        <h3 class="panel-title">Resultados entregados por Usuarios {{$institution}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="start_date2">Fecha inicio</label>
                                    <input type="date" name="start_date2" id="start_date2" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="end_date2">Fecha fin</label>
                                    <input type="date" name="end_date2" id="end_date2" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="submit"  value="Generar" class="btn btn-primary new-btn-primary btn-generar">
                        </div>
                    </div>
                </form>
                <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="height: 200px;">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" id="toastP" style="max-width: 400px !important;">
                        <div class="toast-header" style="color: #000 !important; background-color: rgba(0,0,0,.03);">
                            <strong class="mr-auto">Aviso</strong>
                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            Generando el reporte, esto puede demorar varios minutos. Cuando finalice el reporte estará en "Descargas"...
                        </div>
                    </div>
                </div>
            </div>
            <!-- PILL RESULTADOS POR USUARIOS -->

            <!-- PILL REPORTE REFERENTES/VISITADOR MEDICO -->
            <div class="tab-pane fade tab-personalizada" id="pills-referentes" role="tabpanel" aria-labelledby="pills-referentes-tab">
                <form id="form-referrings" method="POST" action="{{ url('exportReportReferrings') }}" role="form" class="needs-validation" novalidate>
                    {{ csrf_field() }}
                    <div class="card-header">
                        <h3 class="panel-title">Referentes {{$institution}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="referring">Referente</label>
                                    <select class="form-control input-sm" name="select_referrings" id="select_referrings">
                                        <option value="-1" selected disabled>Seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="medical_visitor">Visitador médico</label>
                                    <select class="form-control input-sm" name="select_medical" id="select_medical">
                                        <option value="-1" selected disabled>Seleccione</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="start_date3">Fecha inicio</label>
                                    <input type="date" name="start_date3" id="start_date3" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="end_date3">Fecha fin</label>
                                    <input type="date" name="end_date3" id="end_date3" class="form-control input-sm" required>
                                    <div class="invalid-feedback">
                                        Debe llenar este campo
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <button type="button" class="btn btn-primary btn-form-referrings" style="background-color: #0079C2"><i class="fas fa-search"></i> Buscar</button>
                        <button type="submit" class="btn btn-primary btn-form-export-referrings" style="background-color: #0079C2"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</button>
                    </div>
                </form>
                <!-- <div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center" style="height: 200px;">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" id="toastP" style="max-width: 400px !important;">
                        <div class="toast-header" style="color: #000 !important; background-color: rgba(0,0,0,.03);">
                            <strong class="mr-auto">Aviso</strong>
                             <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            Generando el reporte, esto puede demorar varios minutos. Cuando finalice el reporte estará en "Descargas"...
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- PILL REPORTE REFERENTES/VISITADOR MEDICO -->

        </div>
        <!-- PILLS -->
    </div>

    <!-- SPINNER -->
    <div class="d-flex justify-content-center">
        <div id="spinner" class="spinner-border text-primary loader" style="display: none" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <!-- SPINNER -->

    <!-- DATATABLE -->
    <div class="div-tabla">
        <hr>
        <table id="table-order" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Fecha Realización</th>
                    <th>N° Orden</th>
                    <th>N° Solicitud</th>
                    <th>Cédula</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Email</th>
                    <th>Procedimiento</th>
                    <th>Modalidad</th>
                    <th>Estatus</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody class="center">
                <tr></tr>
            </tbody>
        </table>
    </div>
    <!-- DATATABLE -->

    <!-- DATATABLE REPORTE REFERRINGS -->
    <div class="div-tabla-referrings">
        <hr>
        <table id="table-referrings" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>N° Orden</th>
                    <th>N° Solicitud</th>
                    <th>Paciente</th>
                    <th>Cédula</th>
                    <th>Fecha Realización</th>
                    <th>Referente</th>
                    <th>Email Referente</th>
                    <th>Visitador Médico</th>
                    <th>Email Visitador Médico</th>
                </tr>
            </thead>
            <tbody class="center">
                <tr></tr>
            </tbody>
        </table>
    </div>
    <!-- DATATABLE REPORTE REFERRINGS -->

    <!-- CARD OPCIONES INFORMES -->
    <div class="div-options">  
        <hr>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <fieldset class="form-group">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <legend class="col-form-label col-md-3 pt-0">¿Por qué va a enviar el informe?</legend>
                            <div class="col-md-5">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="1">
                                    <label class="form-check-label" for="gridRadios1">Cambió de correo electrónico el paciente</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="2" checked>
                                    <label class="form-check-label" for="gridRadios2">No se envió el informe</label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="center">
                        <button type="button" class="btn btn-primary btn-send color-primary" data-id="informe"><i class="fas fa-paper-plane"></i> Enviar Informe</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CARD OPCIONES INFORMES -->

    <!-- CARD OPCIONES CEDULA -->
    <div class="div-options1">  
        <hr>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-2"></div>
                        <label for="cedula" class="col-md-3 col-form-label">Cédula errada del paciente</label>
                        <div class="col-md-5">
                            <input type="number" class="form-control" id="cedulavieja" name="cedulavieja">
                            <input type="hidden" class="form-control" id="cedula" name="cedula">
                        </div>
                    </div>
                    <div class="center">
                        <button type="button" class="btn btn-primary btn-send color-primary" data-id="cedula" ><i class="fas fa-paper-plane"></i> Enviar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CARD OPCIONES CEDULA -->

    <!-- MODAL PARA VER INFORMACION DE LA ORDEN -->
    <div class="modal fade bd-data-modal-lg" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dataModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="form-group col">
                                <label for="historia">No. Historia</label>
                                <input type="text" class="form-control" id="historia" name="historia" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="cedula">Cédula</label>
                                <input type="text" class="form-control" id="cedula" name="cedula" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="fecha_nacimiento">Fecha nacimiento</label>
                                <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="edad">Edad</label>
                                <input type="text" class="form-control" id="edad" name="edad" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sexo">Sexo</label>
                                <input type="text" class="form-control" id="sexo" name="sexo" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="peso">Peso</label>
                                <input type="text" class="form-control" id="peso" name="peso" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="altura">Altura</label>
                                <input type="text" class="form-control" id="altura" name="altura" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="telefonos">Telefonos</label>
                                <input type="text" class="form-control" id="telefonos" name="telefonos" readonly>
                            </div>
                        </div>
                        <hr>
                        <h5 class="modalidad text-center"></h5>
                        <div class="row">
                            <div class="form-group col">
                                <label for="fecha">Fecha realización</label>
                                <input type="text" class="form-control" id="fecha" name="fecha" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="orden">No. de orden</label>
                                <input type="text" class="form-control" id="orden" name="orden" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="solicitud">No. de solicitud</label>
                                <input type="text" class="form-control" id="solicitud" name="solicitud" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="tipo_paciente">Tipo de paciente</label>
                                <input type="text" class="form-control capitalize" id="tipo_paciente" name="tipo_paciente" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="subtipo_paciente">Subtipo de paciente</label>
                                <input type="text" class="form-control" id="subtipo_paciente" name="subtipo_paciente" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="estatus">Estatus</label>
                                <input type="text" class="form-control" id="estatus" name="estatus" readonly>
                            </div>
                        </div>
                        <hr>
                        <div id="botones" class="float-right"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL PARA VER INFORMACION DE LA ORDEN -->

    <!-- MODAL PARA VER EL RESULTADO EXITOSO DEL ENVÍO DE LA ORDEN -->
    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="myModalLabel">Éxito</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    El proceso fue realizado correctamente
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL PARA VER EL RESULTADO EXITOSO DEL ENVÍO DE LA ORDEN -->

    <!-- MODAL PARA VER EL RESULTADO ERRONEO DEL ENVÍO DE LA ORDEN -->
    <div id="myModal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="myModalLabel">Error</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    Ocurrió un error al intentar realizar el proceso. Inténtelo nuevamente.
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL PARA VER EL RESULTADO ERRONEO DEL ENVÍO DE LA ORDEN -->

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" defer></script>
    <script src="http://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js" defer></script>
    <script>
        $(document).ready(function(){

            // hiden the alert
            setTimeout(function(){
                $(".alert").alert('close');
            }, 3000);

            $(".btn-generar").click(function(){
                $('#toastP').toast('show')
                $("#backdrop").css("display", "block");
                setTimeout(function(){
                    $("#backdrop").css("display", "none");
                    $('#toastP').toast('hide')
                }, 5000);
            });

            // Bloquear tecla "enter"
            $("input").keydown(function (e){
                // Capturamos qué telca ha sido
                var keyCode= e.which;
                // Si la tecla es el Intro/Enter
                if (keyCode == 13){
                    // Evitamos que se ejecute eventos
                    event.preventDefault();
                    // Devolvemos falso
                    return false;
                }
            });

            // Ocultar tabla si entro en el pills del reporte
            $('#pills-reporte-tab, #pills-cedula-tab, #pills-informe-tab, #pills-auditoria-tab, #pills-resultados-tab, #pills-referentes-tab').click(function(){
                document.getElementById("orden").value = '';
                document.getElementById("orden1").value = '';
                $(".div-tabla").css("visibility", "hidden");
                $(".div-tabla-referrings").css("visibility", "hidden");
                $(".div-options").css("visibility", "hidden");
                $(".div-options1").css("visibility", "hidden");
            })

            // Setear fecha a los inputs tab1
            $('.tab-1').click(function(e){
                setFechas("start_date", "end_date");
            });

            $("#start_date").change(function(){
                validarFechas("start_date", "end_date", "#fecha_inicio", "inicio");
            });

            $("#end_date").change(function(){
                validarFechas("end_date", "start_date", "#fecha_fin", "fin");
            });
            // Setear fecha a los inputs tab1


            // Setear fecha a los inputs tab2
            $('.tab-2').click(function(e){
                setFechas("start_date1", "end_date1");
            });

            $("#start_date1").change(function(){
                validarFechas("start_date1", "end_date1", "#fecha_inicio", "inicio");
            });

            $("#end_date1").change(function(){
                validarFechas("end_date1", "start_date1", "#fecha_fin", "fin");
            });
            // Setear fecha a los inputs tab2


            // Setear fecha a los inputs tab3
            $('.tab-3').click(function(e){
                setFechas("start_date2", "end_date2");
            });

            $("#start_date2").change(function(){
                validarFechas("start_date2", "end_date2", "#fecha_inicio", "inicio");
            });

            $("#end_date2").change(function(){
                validarFechas("end_date2", "start_date2", "#fecha_fin", "fin");
            });
            // Setear fecha a los inputs tab3


            // Setear fecha a los inputs tab4
            $('.tab-4').click(function(e){
                setFechas("start_date3", "end_date3");
            });

            $("#start_date3").change(function(){
                validarFechas("start_date3", "end_date3", "#fecha_inicio", "inicio");
            });

            $("#end_date3").change(function(){
                validarFechas("end_date3", "start_date3", "#fecha_fin", "fin");
            });
            // Setear fecha a los inputs tab4


            $('.dropdown-toggle').dropdown();
            $('[data-toggle="tooltip"]').tooltip();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#orden, #orden1").blur(function(){
                $(".div-tabla").css("visibility", "hidden");
                $(".div-tabla-referrings").css("visibility", "hidden");
                $(".div-options").css("visibility", "hidden");
                $(".div-options1").css("visibility", "hidden");
            });


            $('.btn-form-cedula').click(function(e){
                if(document.getElementById("orden1").value != ''){
                    e.preventDefault();
                    llenarTabla('.div-options1', '#form-cedula');
                }else{
                    alert("Debe indicar el número de orden")
                }
            })


            $('.btn-form').click(function(e){
                if(document.getElementById("orden").value != ''){
                    e.preventDefault();
                    llenarTabla('.div-options', '#form-search');
                }else{
                    alert("Debe indicar el número de orden")
                }
            });

            $('#dataModal').on('show.bs.modal', function (event) {
                
                var button = $(event.relatedTarget)

                var nombres = button.data('nombres')
                var apellidos = button.data('apellidos')
                var sexo = button.data('sexo')
                var cedula = button.data('cedula')
                var fecha_nacimiento = button.data('fecha_nacimiento')
                var numero_historia = button.data('numero_historia')
                var peso = button.data('peso')
                var altura = button.data('altura')
                var cellphone_number = button.data('cellphone_number')
                var telephone_number = button.data('telephone_number')
                var accession_number = button.data('accession_number')
                var id_paciente = button.data('id_paciente')
                var modalidad = button.data('modalidad')
                var descripcion = button.data('descripcion')
                var fecharealizacion = button.data('fecharealizacion')
                var id_solicitud = button.data('id_solicitud')
                var estatus = button.data('estatus')
                var id_estatus = button.data('id_estatus')
                var patient_type = button.data('patient_type')
                var informe = button.data('informe')
                var addendum = button.data('addendum')

                 // Asiganar valores a la modal 
                var modal = $(this)
                modal.find('.modal-title').text(apellidos + ", " + nombres)
                modal.find('.modal-body #historia').val(numero_historia)
                modal.find('.modal-body #cedula').val(cedula)
                modal.find('.modal-body #fecha_nacimiento').val(formatFecha(fecha_nacimiento, true))
                modal.find('.modal-body #edad').val(calcularEdad(fecha_nacimiento))
                modal.find('.modal-body #sexo').val(sexo == 'F' ? 'Femenino' : 'Masculino')
                modal.find('.modal-body #peso').val(peso == 0 ? "S/E" : parseFloat(peso).toFixed(2))
                modal.find('.modal-body #altura').val(altura == 0 ? "S/E" : parseFloat(altura).toFixed(2))
                modal.find('.modal-body #telefonos').val(formatearDatos(cellphone_number, telephone_number, 'telefono'))
                modal.find('.modal-body #fecha').val(formatFecha(fecharealizacion, true, true))
                modal.find('.modal-body #orden').val(accession_number)
                modal.find('.modal-body #solicitud').val(id_solicitud)
                modal.find('.modal-body #estatus').val(estatus)
                modal.find('.modal-body .modalidad').text(descripcion + "(" + modalidad + ")")
                modal.find('.modal-body #tipo_paciente').val(patient_type)

                if(id_estatus == '2' || id_estatus == '3'){
                    modal.find('.modal-body .btn-informe').hide()
                }
                
                modal.find('.modal-body #botones').empty()

                // Boton para descargar el informe de la orden
                if (informe){
                    url = '<a href="{{ url('searchReport/#accession')}}" target="_blank" class="btn btn-secondary btn-lg active btn-informe" role="button" aria-pressed="true" data-toggle="tooltip" data-placement="top" title="Descargar Informe" style="margin-right: 5px;"><i class="fas fa-clipboard-list"></i></a>';
                    url = url.replace("#accession", accession_number + '/O');
                    modal.find('.modal-body #botones').append(url)
                }

                // Boton para descargar el addendum de la orden
                if(addendum){
                    urlA = '<a href="{{ url('searchReport/#accession')}}" target="_blank" class="btn btn-secondary btn-lg active btn-informe" role="button" aria-pressed="true" data-toggle="tooltip" data-placement="top" title="Descargar Addendum" style="margin-right: 5px;"><i class="fas fa-clipboard-list"></i></a>';
                    urlA = urlA.replace("#accession", accession_number + '/A');
                    modal.find('.modal-body #botones').append(urlA)
                }
            })
            
            $('.btn-send').click(function(e){

                if($(this).data("id") == 'cedula'){
                    if(document.getElementById("cedula").value == document.getElementById("cedulavieja").value){
                        alert("La cédula errada es igual a la cédula nueva")
                        return
                    }
                    var data = {};
                    data['orden'] = document.getElementById("orden1").value
                    data['cedulanueva'] = document.getElementById("cedula").value
                    data['cedulavieja'] = document.getElementById("cedulavieja").value
                    url = "changeID"
                }else{
                    var data = {};
                    data['orden'] = document.getElementById("orden").value
                    data['opcion'] = $('input:radio[name=gridRadios]:checked').val()
                    url = "sendReport"
                }

                $("#backdrop").css("display", "block");
                $("#spinner").css("display", "block");

                $.ajax({
                    context: this,
                    async: true,
                    type: "POST",
                    data: data,
                    url: url,
                    success: function(response){
                        $("#spinner").css("display", "none");
                        $("#backdrop").css("display", "none");
                        if(response['status'] == '200' || response['status'] == '201'){
                            if(url == 'changeID'){
                                document.getElementById("cedula").value = '';
                            }
                            $("#myModal2").modal("show");
                        }else{
                            $('#myModal3').modal('show')
                        }
                    },
                    error: function() {}
                });
            });

            function validarFechas(inicio, fin, div, tipo){
                if(tipo == 'inicio'){
                    if($("#"+inicio).val() > $("#"+fin).val()){
                        
                        $(div).css("display", "block");
                        setTimeout(function(){
                            $(div).css("display", "none");
                        }, 2000);

                        document.getElementById(inicio).value = $("#"+fin).val();
                    }
                }else{
                    if($("#"+inicio).val() < $("#"+fin).val()){
                        
                        $(div).css("display", "block");
                        setTimeout(function(){
                            $(div).css("display", "none");
                        }, 2000);

                        document.getElementById(inicio).value = $("#"+fin).val();
                    }
                }
            }

            function setFechas(inicio, fin){
                today = formatFecha();

                document.getElementById(inicio).value = today;
                document.getElementById(fin).value = today;
                $("#"+inicio, "#"+fin).attr({
                    "max" : today
                });
            }

            function formatFecha(){
               
                var date = new Date()
                var day = date.getDate()
                var month = date.getMonth() + 1
                var year = date.getFullYear()

                month = month < 10 ? '0' + month : month
                day = day < 10 ? '0' + day : day

                return year+'-'+month+'-'+day
            }
            // Setear fecha a los inputs


            function formatearDatos(dato1, dato2, texto){

                var info = "";

                if(texto == 'telefono'){
                    if(dato1){
                        info = dato1;
                        if(dato2){
                            info += " / " + dato2;
                        }
                    }else{
                        if(dato2){
                            info = dato2;
                        }else{
                            info = "S/E";
                        }
                    }
                }else{
                    if(dato1 == 0){
                        info = "S/E";
                    }else{
                        info = parseFloat(dato1).toFixed(2);
                    }
                }

                return info;
            }


            function traducirStatus(status){
                if(status == 'to-admit'){
                    status = 'Por admitir'
                }else if(status == 'to-make'){
                    status = 'Por realizar'
                }else if(status == 'to-do'){
                    status = 'Por realizar'
                }else if(status == 'to-dictate'){
                    status = 'Por dictar'
                }else if(status == 'to-transcribe'){
                    status = 'Por transcribir'
                }else if(status == 'to-approve'){
                    status = 'Por aprobar'
                }else if(status == 'finished'){
                    status = 'Terminada'
                }else if(status == 'suspended'){
                    status = 'Suspendida'
                }

                return status
            }


            function formatFecha(fecha = '', ordenado = '', hora = ''){
                if(fecha){
                    var date = new Date(fecha)
                }else{
                    var date = new Date()
                }

                var day = date.getDate()
                var month = date.getMonth() + 1
                var year = date.getFullYear()

                month = month < 10 ? '0' + month : month
                day = day < 10 ? '0' + day : day

                if(ordenado){
                    if(hora){
                        hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
                        minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
                        ampm = hours >= 12 ? 'PM' : 'AM';

                        return day+'-'+month+'-'+year+' '+hours+':'+minutes+' '+ampm
                    }else{
                        return day+'-'+month+'-'+year
                    }
                }else{
                    return year+'-'+month+'-'+day
                }
            }


            function calcularEdad(fecha) {
                var hoy = new Date();
                var cumpleanos = new Date(fecha);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }

                return edad;
            }


            function llenarTabla(div, form){

                $(".div-tabla").css("visibility", "visible");
                $(div).css("visibility", "visible");
                $("#backdrop").css("display", "block");
                $("#spinner").css("display", "block");

                var dataString = $(form).serializeArray();
                var data = {};
                $(dataString).each(function(index, obj){
                    data[obj.name] = obj.value;
                });

                table = $('#table-order').DataTable({
                    destroy: true,
                    serverSide: false,
                    stateSave: true,
                    autoWidth: false,
                    aaSorting: [0, 'asc'], //Ordenar por columna 
                    ajax: {
                        method: 'POST',
                        url: 'ajaxRequest',
                        data: data,
                        async: true,
                        complete: function(data){
                            $("#spinner").css("display", "none");
                            $("#backdrop").css("display", "none");
                            if(data['responseJSON']['aaData'].length == 0 || data['responseJSON']['aaData'][0]['status'] != 'Terminada'){
                                $(div).css("visibility", "hidden");
                            }else{
                                $(div).css("visibility", "visible");
                            }
                        }
                    },
                    columns: [
                        {data: "issue_date"},
                        {data: "accession_number"},
                        {data: "id"},
                        {data: "patient_identification_id"},
                        {data: "patient_first_name"},
                        {data: "patient_last_name"},
                        {data: "email"},
                        {data: "description"},
                        {data: "name"},
                        {
                            data: null,
                            bSortable: false,
                            mRender: function(data){

                                if(form == '#form-cedula'){
                                    $("#cedula").val(data.patient_identification_id);
                                }

                                data.status = traducirStatus(data.status)
                                
                                var html = '<span>'+data.status+'</span>';

                                if(data.requested_procedure_status_id == '1'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 16%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '2'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 33%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '3'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '4'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 66%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '5'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 83%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '6'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else{
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }

                                return html; 
                            }
                        },
                        {
                            data: null,
                            bSortable: false,
                            mRender: function(data){

                                var btn = '';

                                btn += '<button type="button" class="btn btn-primary" style="margin-top: 7px;" data-toggle="modal" data-target=".bd-data-modal-lg" data-accession_number="'+data.accession_number+'" data-nombres="'+data.patient_first_name+'" data-apellidos="'+data.patient_last_name+'" data-sexo="'+data.sexo+'" data-cedula="'+data.patient_identification_id+'" data-fecha_nacimiento="'+data.birth_date+'" data-numero_historia="'+data.additional_patient_history+'" data-peso="'+data.weight+'" data-altura="'+data.height+'" data-cellphone_number="'+data.cellphone_number+'" data-telephone_number="'+data.telephone_number+'" data-id_paciente="'+data.id+'" data-descripcion="'+data.description+'" data-modalidad="'+data.name+'" data-datos="'+data+'" data-fecharealizacion="'+data.issue_date+'" data-id_solicitud="'+data.id+'" data-estatus="'+data.status+'" data-id_estatus="'+data.requested_procedure_status_id+'" data-patient_type="'+data.patient_type+'" data-informe="'+data.informe+'" data-addendum="'+data.addendum+'" title="Ver Orden"><i class="fas fa-search"></i></button>';

                                return btn;
                            }
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0, render:function(data){ 
                                return formatFecha(data, true, true);
                            }
                        }
                    ],
                    language: {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "No se encontró registro con el número de orden indicado",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            }

            $('#pills-referentes-tab').click(function(){

                $.ajax({
                    type: 'GET',
                    url: "fillSelect/referrings",
                    success:function(response){
                        if(response.length > 0){
                            fillSelect(response, '#select_referrings')
                        }
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: "fillSelect/medical_visitors",
                    success:function(response){
                        if(response.length > 0){
                            fillSelect(response, '#select_medical')
                        }
                    }
                });
            })

            function fillSelect(response, select){
                $(select).empty();
                $(select).append("<option value='-1' selected disabled>Seleccione</option>");
                for(var i = 0; i < response.length; i++){
                    $(select).append("<option value='"+response[i]['id']+"'>"+response[i]['first_name'] + " " + response[i]['last_name']+"</option>");
                }
            }

            $('.btn-form-referrings').click(function(e){

                e.preventDefault();

                $(".div-tabla-referrings").css("visibility", "visible");
                $("#backdrop").css("display", "block");
                $("#spinner").css("display", "block");

                var dataString = $('#form-referrings').serializeArray();
                var data = {};
                $(dataString).each(function(index, obj){
                    data[obj.name] = obj.value;
                });

                table = $('#table-referrings').DataTable({
                    destroy: true,
                    serverSide: false,
                    stateSave: true,
                    autoWidth: false,
                    aaSorting: [0, 'asc'], //Ordenar por columna 
                    ajax: {
                        method: 'POST',
                        url: 'ajaxRequestReferrings',
                        data: data,
                        async: true,
                        complete: function(){
                            $("#spinner").css("display", "none");
                            $("#backdrop").css("display", "none");
                        }
                    },
                    columns: [
                        {data: "orden"},
                        {data: "id"},
                        {data: "patient"},
                        {data: "patient_identification_id"},
                        {data: "issue_date"},
                        {data: "referring"},
                        {data: "email_referring"},
                        {data: "medical"},
                        {data: "email_medical"}
                    ],
                    language: {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            });
        });
    </script>

@endsection
