<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Reporte Facturación</title>

    </head>
    <body>
        <script type="text/php">
            $size = 8;
            $y = 25;
            $x = $pdf->get_width() - 80;
            $font = $fontMetrics->get_font("sans-serif");
            $pdf->page_text($x, $y, " Página {PAGE_NUM}/{PAGE_COUNT}", $font, $size);
        </script>
        <main>
            <h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
            <table border = 1 cellspacing = 0 cellpadding = 0>
                <thead>
                    <tr>
                        <th># Orden</th>
                        <th>Cédula</th>
                        <th>Procedimiento/Estudio</th>    
                        <th>Estatus</th> 
                        <th>Viewmed</th>
                        <th>Informe Viewmed</th>
                        <th>Fecha creado</th>  
                        <th>Fecha culminado</th>   
                        <th>Observación</th>   
                     </tr>
                </thead>
                <tbody>
                    @foreach($result as $info)  
                        <tr>
                            <td style='text-align:center; font-size: 12px'>{{$info->id}}</td>
                            <td style='text-align:center; font-size: 12px'>{{$info->patient_identification_id}}</td>
                            <td style='text-align:center; font-size: 12px'>{{$info->description}} ({{$info->modalidad}})</td>
                            <td style='text-align:center; font-size: 12px'>
                                {{$info->status}}
                                @if($info->suspension_reason_id != null)
                                    ({{$info->suspension}})
                                @endif
                            </td>
                            <td style='text-align:center; font-size: 12px'>
                                @if($info->viewmed == true)
                                    SI
                                @else
                                    NO
                                @endif
                            </td>
                            <td style='text-align:center; font-size: 12px'>{{$info->report}}</td>
                            <td style='text-align:center; font-size: 12px'>{{$info->created_at}}</td>
                            <td style='text-align:center; font-size: 12px'>{{$info->culmination_date}}</td>
                            <td style='text-align:center; font-size: 12px'>
                                @if($info->cedula == 'N')
                                    La orden no esta aun en Viewmed
                                @elseif($info->cedula == 'S')

                                @elseif($info->cedula == 'E')
                                    La cédula no coincide ({{$info->cedulaViewmed}})
                                @elseif($info->cedula == 'P')
                                    Orden de prueba
                                @endif
                            </td>
                        </tr>
                   @endforeach
                </tbody>
            </table>
        </main>
    </body>
</html>