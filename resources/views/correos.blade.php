<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Reporte Correo de Usuarios</title>
        <style>
            html, body {
                font-family: Arial, Helvetica;
            }

            body {
                margin-bottom: 1.5cm;
            }

            table {
                width: 100%;
            }
            /*td {
                white-space: nowrap;
            }*/
            td.label {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <script type="text/php">
            $size = 8;
            $y = 25;
            $x = $pdf->get_width() - 80;
            $font = $fontMetrics->get_font("sans-serif");
            $pdf->page_text($x, $y, " Página {PAGE_NUM}/{PAGE_COUNT}", $font, $size);
        </script>
        <main>
            <h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
            <h4>Ordenes realizadas por usuarios</h4>
            
            <table border = 1 cellspacing = 0 cellpadding = 0 style="margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Ordenes realizadas</th>
                     </tr>
                </thead>
                <tbody>
                    @foreach($usuarios as $usuario)  
                        <tr>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$usuario['usuario']}}</td>
                            <td style='text-align:center; font-size: 14px; padding: 5px;'>{{$usuario['ordenes']}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <hr>

            <table border = 1 cellspacing = 0 cellpadding = 0 style="margin-top: 10px">
                <thead>
                    <tr>
                        <th>Número de solicitud</th>
                        <th>Paciente</th>
                        <th>Cédula</th>
                        <th>Telefonos</th>    
                        <!-- <th>Celular</th>  -->
                        <th>Email</th> 
                        <th>Creado por</th>
                     </tr>
                </thead>
                <tbody>
                    @foreach($result as $info)  
                        <tr>
                            <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->id}}</td>
                            <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->patient_first_name}} {{$info->patient_last_name}}</td>
                            <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->patient_ID}}</td>
                            <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->telephone_number}} / {{$info->cellphone_number}} </td>
                            <!-- <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->cellphone_number}}</td> -->
                            <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->email}}</td>
                            <td style='text-align:center; font-size: 12px; padding: 5px;'>{{$info->first_name}} {{$info->last_name}}</td>
                        </tr>
                   @endforeach
                </tbody>
            </table>
        </main>
    </body>
</html>