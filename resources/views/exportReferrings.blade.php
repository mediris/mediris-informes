<h4>Reporte desde {{$start_date}} al {{$end_date}}</h4>
<br>
<br>
<table>
    <thead>
        <tr>
            <th style='text-align:center; font-weight: bold;'>N° Orden</th>
            <th style='text-align:center; font-weight: bold;'>N° Solicitud</th>
            <th style='text-align:center; font-weight: bold;'>Paciente</th>
            <th style='text-align:center; font-weight: bold;'>Cédula</th>
            <th style='text-align:center; font-weight: bold;'>Fecha Realización</th>
            <th style='text-align:center; font-weight: bold;'>Referente</th>
            <th style='text-align:center; font-weight: bold;'>Email Referente</th>
            <th style='text-align:center; font-weight: bold;'>Visitador Médico</th>
            <th style='text-align:center; font-weight: bold;'>Email Visitador Médico</th>
         </tr>
    </thead>
    <tbody>
        @foreach($result as $info)
            <tr>
                <td style='text-align:center;'>{{$info->orden}}</td>
                <td style='text-align:center;'>{{$info->id}}</td>
                <td style='text-align:center;'>{{$info->patient}}</td>
                <td style='text-align:center;'>{{$info->patient_identification_id}}</td>
                <td style='text-align:center;'>{{$info->issue_date}}</td>
                <td style='text-align:center;'>{{$info->referring}}</td>
                <td style='text-align:center;'>{{$info->email_referring}}</td>
                <td style='text-align:center;'>{{$info->medical}}</td>
                <td style='text-align:center;'>{{$info->email_medical}}</td>
            </tr>
       @endforeach
    </tbody>
</table>