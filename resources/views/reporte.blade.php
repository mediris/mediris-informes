<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="titleext/html; charset=utf-8"/>
        <title>{{ $accession_number }}</title>
        <style>
            html, body {
                font-family: Arial, Helvetica;
            }

            body {
                margin-top: 7cm;
                margin-bottom: 1.5cm;
            }

            table {
                width: 100%;
            }
            td {
                white-space: nowrap;
            }

            td.label {
                font-weight: bold;
            }

            .signature {
                img {
                    max-height: 50px;
                }
                .radiologist {
                    font-size: 12px;
                    font-weight: bold;      
                }
                .technician {
                    margin-top: 15px;
                    font-size: 10px;
                }
                .date {
                    font-size: 10px;
                }
            }

            .header-img {
                width: 725px;
                padding: 0;
                margin: 0;
            }

            .footer-img {
                width: 725px;
                padding: 0;
                margin: 0;
            }


            .header {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
            }

            .footer {
                position: fixed;
                left: 0px;
                right: 0px;
                bottom: 4.5cm;
                padding-top: 10px;
            }

            .page-number {
                position: absolute;
                top: -50px;
                right: 0px;
            }
        </style>
    </head>
    <body id="app-layout">
        <script type="text/php">
            $size = 8;
            $y = 10;
            $x = $pdf->get_width() - 65;
            $font = $fontMetrics->get_font("sans-serif");
            $pdf->page_text($x, $y, " Página {PAGE_NUM}/{PAGE_COUNT}", $font, $size);
        </script>
        <div class="header">
            @if($header_img)
                <img src="{{ $header_img }}" class="header-img">
            @endif
            <table>
                <tr>
                    <td style="font-weight: bold">FECHA:</td>
                    <td style="text-transform: capitalize;">{{ $fecha_creacion }}</td>
                    <td style="font-weight: bold">TIPO:</td>
                    <td style="text-transform: capitalize;">{{ $patient_type }}</td>
                    <td style="font-weight: bold">INFORME:</td>
                    <td>{{ $accession_number }}</td>
                </tr>

                <tr>
                    <td style="font-weight: bold">PACIENTE:</td>
                    <td colspan="3">{{ $nombres }}  {{ $apellidos }}</td>
                    <td style="font-weight: bold">C.I:</td>
                    <td>V.- {{ $cedula }}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">FEC. NAC:</td>
                    <td colspan="3">{{ $fecha_nacimiento }} ({{ $edad }} años)</td>
                    <td style="font-weight: bold">SEXO:</td>
                    <td>{{ $sexo }}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold" >REFERENCIA:</td>
                    <td colspan="5">{{ $referido }}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">ESTUDIO:</td>
                    <td colspan="5">{{ $descripcion }}</td>
                </tr>
            </table>
        </div>

        <div class="footer">
            <div class="signature">
                <img src="{{ $firma }}" height="100" width="450">
                <div class="radiologist">{{ $doctor }} - {{ $especialidad }}</div>
                @if(isset($mpps))
                    <div class="credeniales">C.I. {{$ci}} / {{$mpps}} / {{$cdmc}}</div>
                @endif
                <div class="date">{{ $fecha_dictado }}</div>
            </div>
            @if($footer_img)
                <!-- <p>{{str_random(40)}}</p>  -->
                <img src="{{ $footer_img }}" class="footer-img">
            @endif
        </div>

        <div class="content" style="text-align: justify;">
            {!! $texto !!}
        </div>
    </body>
</html>

<style>
    @if($firma)
        .footer {
            bottom: 4.5cm;
        }
        body {
            margin-bottom: 3cm;
        }
    @else
        .footer {
            bottom: 2cm;
        }
        body {
            margin-bottom: 1.5cm;
        }
    @endif

    .footer p{

        text-align: right;
        color: #C2C2C2;
    }

    .label{
        font-size: 13px;
    }

    td{
        font-size: 13px;
    }

    .content{

        font-size: 13px;
        text-align: justify!important;

    }

    .header-img{
        max-height: 120px;
    }
    .header{
        margin-bottom: 1px!important;
    }
    body{

        margin-top: 250px!important;
        margin-bottom: 220px!important;
        font-size: 13px;
    }

    .footer{

        margin-top: 0px!important;
        margin-bottom: 100px!important;
    }
    .firma{

        width: 400px;
        min-height: 80px;
    }

</style>