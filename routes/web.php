<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::group([ 'prefix' => '/', 'middleware' => 'auth' ], function () {
    
    Route::get('home', [ 'as' => 'home', 'uses' => 'HomeController@index' ]);

    Route::post('ajaxRequest', [ 'as' => 'ajaxRequest', 'uses' => 'HomeController@ajaxRequestPost' ]);
    
    Route::post('sendReport', [ 'as' => 'sendReport', 'uses' => 'HomeController@sendReport' ]);

    Route::get('searchReport/{id}/{t}', [ 'as' => 'searchReport/{id}/{t}', 'uses' => 'HomeController@searchReportPost' ]);

    Route::post('reporte', [ 'as' => 'reporte', 'uses' => 'HomeController@reportViewmed' ]);

    Route::post('changeID', [ 'as' => 'changeID', 'uses' => 'HomeController@changeID' ]);

    Route::post('reporteAuditoria', [ 'as' => 'reporte', 'uses' => 'HomeController@reporteAuditoria' ]);

    Route::post('results', [ 'as' => 'results', 'uses' => 'HomeController@findDelivers' ]);

    Route::get('fillSelect/{table}', [ 'as' => 'fillSelect/{table}', 'uses' => 'HomeController@fillSelect' ]);
    
    Route::post('ajaxRequestReferrings', [ 'as' => 'ajaxRequestReferrings', 'uses' => 'HomeController@findOrdersReferringsMedicalVisitors' ]);
    
    Route::post('exportReportReferrings', [ 'as' => 'exportReportReferrings', 'uses' => 'HomeController@exportReportReferrings' ]);
});

