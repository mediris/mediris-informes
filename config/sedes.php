<?php

return [

    'data' => [
        
        // DESARROLLO
        '99' => [
            'url' => 'https://medirisdev.meditron.com.ve/storage/',
            'name' => 'DEV',
            'facturacion' => true,
            'id_institution_viewmed' => ''
        ],

        // CIENCIAS
        '04' => [
            'url' => 'https://medirisclc.idaca.com.ve/storage/',
            'name' => 'Las Ciencias',
            'facturacion' => true,
            'id_institution_viewmed' => '5fc044c3a2be3a1fa623faf2'
        ], 
        
        // CMDLT
        '11' => [
            'url' => 'https://mediriscdt.idaca.com.ve/storage/',
            'name' => 'CMDLT',
            'facturacion' => true,
            'id_institution_viewmed' => '5f6cd01f9e80a3612d70ead4'
        ],
        
        // MNU
        '12' => [
            'url' => 'https://medirismnu.idaca.com.ve/storage/',
            'name' => 'MNU',
            'facturacion' => true,
            'id_institution_viewmed' => '5f6cd01f9e80a3612d70ead4'
        ],
        
        // OASIS
        '16' => [
            'url' => 'https://medirisoas.idaca.com.ve/storage/',
            'name' => 'Oasis',
            'facturacion' => true,
            'id_institution_viewmed' => '5fc04463a2be3a1fa623faf1'
        ],
        
        // SANATRIX
        '19' => [
            'url' => 'https://medirissnx.idaca.com.ve/storage/',
            'name' => 'La Sanatrix',
            'facturacion' => true,
            'id_institution_viewmed' => '5f6a175e9807fc0c4f63600d'
        ],
        
        // FLORESTA
        '20' => [
            'url' => 'https://medirisflt.idaca.com.ve/storage/',
            'name' => 'La Floresta',
            'facturacion' => true,
            'id_institution_viewmed' => '5e441a69a045161b2515c27f'
        ],
        
        // FENIX
        '21' => [
            'url' => 'https://medirisfnx.idaca.com.ve/storage/',
            'name' => 'Fénix',
            'facturacion' => true,
            'id_institution_viewmed' => '612e984a68541f7c7b8aef22'
        ],
        
        // ARBOLEDA
        '22' => [
            'url' => 'https://medirisabl.idaca.com.ve/storage/',
            'name' => 'La Arboleda',
            'facturacion' => true,
            'id_institution_viewmed' => '61155f23bf18d14a9a5db36e'
        ],
        
        // HIGUEROTE
        '23' => [
            'url' => 'https://medirishig.idaca.com.ve/storage/',
            'name' => 'Higuerote',
            'facturacion' => true,
            'id_institution_viewmed' => '61ad0663b08e6a15ac7d659f'
        ],

        // LA VIÑA
        '25' => [
            'url' => 'https://mediriscpv.idaca.com.ve/storage/',
            'name' => 'La Viña',
            'facturacion' => true,
            'id_institution_viewmed' => '670296968bc4da38a15d4f1d'
        ],

        // AVILA UNIDAD DE LA MUJER
        '01' => [
            'url' => 'https://mediris.clinicaelavila.com/storage/',
            'name' => 'El Ávila',
            'facturacion' => false,
            'id_institution_viewmed' => '64087c74af4f1aaa49670776'
        ],

        // AVILA UNIDAD DE IMAGEN
        '900' => [
            'url' => 'https://mediris.clinicaelavila.com/storage/',
            'name' => 'El Ávila',
            'facturacion' => false,
            'id_institution_viewmed' => '64087c74af4f1aaa49670776'
        ],

        // AVILA UNIDAD DE ULTRASONIDO
        '901' => [
            'url' => 'https://mediris.clinicaelavila.com/storage/',
            'name' => 'El Ávila',
            'facturacion' => false,
            'id_institution_viewmed' => '64087c74af4f1aaa49670776'
        ],

        // AVILA ECOCARDIOGRAFIA
        '902' => [
            'url' => 'https://mediris.clinicaelavila.com/storage/',
            'name' => 'El Ávila',
            'facturacion' => false,
            'id_institution_viewmed' => '64087c74af4f1aaa49670776'
        ],

        // LABORATORIO CLINICO EFICIENTE
        '02' => [
            'url' => 'https://mediris.solumedica.com.ve/storage/',
            'name' => 'Solumedica',
            'facturacion' => false,
            'id_institution_viewmed' => '656b846e3e557d2c9e0e0dff'
        ]
    ]
];

?>