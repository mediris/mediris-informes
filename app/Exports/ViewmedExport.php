<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class ViewmedExport implements FromView, ShouldAutoSize{

    use Exportable;

    public function __construct($data, $start_date, $end_date, $fact){
        $this->data = $data;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->fact = $fact;
    }

    public function view(): View{
        return view('exportFacturacion', [
            'result' => $this->data, 
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'fact' => $this->fact
        ]);
    }

}