<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Exports\ResultsExport;
use App\Exports\CorreosExport;
use App\Exports\ViewmedExport;
use App\Exports\ReferringsExport;


class HomeController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){

        $institucion = $this->getDataInstitution('administrative_id');

        return view('home', ['institution'=> $this->getDataConf($institucion, 'name')]);
    }


    public function ajaxRequestPost(Request $request){

        $datos = $request->all();

        if(isset($datos['orden'])){
            $orden = $datos['orden'];
        }else{
            $orden = $datos['orden1'];
        }

        $sql = "SELECT T1.id as accession_number, T1.service_request_id, T1.procedure_id, T1.requested_procedure_status_id, T2.issue_date, T2.id, T2.patient_id, T2.patient_identification_id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.weight, T2.height, T2.patient_type_id, T3.administrative_ID as sexo, T4.birth_date, T4.additional_patient_history, T4.cellphone_number, T4.telephone_number, T4.email, T5.description, T5.modality_id, T6.name, T7.description as status, T8.description as patient_type FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`procedures` T5 ON(T5.id = T1.procedure_id) LEFT JOIN `apimeditron`.`modalities` T6 ON(T6.id = T5.modality_id) LEFT JOIN `apimeditron`.`requested_procedure_statuses` T7 ON(T7.id = T1.requested_procedure_status_id) LEFT JOIN `apimeditron`.`patient_types` T8 ON(T8.id = T2.patient_type_id) WHERE T1.id = '".$orden."'";

        $info = DB::select($sql);

        if(count($info) > 0){
            $info[0]->addendum =  $this->validarAddendums($orden);
            $info[0]->informe =  $this->validarInforme($orden);
            $result = array('aaData'=>$info);
        }else{
            $result = array('aaData'=>[]);
        }

        $data = json_encode($result);

        return $data;
    }


    public function searchReportPost($id, $t){

        date_default_timezone_set('America/Caracas');
        setlocale(LC_TIME, 'es_ES.UTF-8');
        setlocale(LC_TIME, 'spanish');

        $institucion = $this->getDataInstitution('administrative_id');

        $url = $this->getDataConf($institucion, 'url');

        if($t == 'O'){

            // $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.text, T1.radiologist_user_name, T1.radiologist_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.radiologist_user_id) LEFT JOIN prefixes T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN institutions T10 ON(T10.id = '1') WHERE T1.id = '".$id."'";

            $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.text, T1.approve_user_name, T1.approve_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T2.institution_id, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T7.mpps, T7.cdmc, T7.administrative_id as ci, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN `mediris`.`sexes` T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN `mediris`.`patients` T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN `mediris`.`users` T7 ON(T7.id = T1.approve_user_id) LEFT JOIN `mediris`.`prefixes` T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN `mediris`.`institutions` T10 ON(T10.id = T2.institution_id) WHERE T1.id = '".$id."'";
        }else{

            // $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.radiologist_user_name, T1.radiologist_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer, T11.text FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.radiologist_user_id) LEFT JOIN prefixes T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN institutions T10 ON(T10.id = '1') LEFT JOIN `apimeditron`.`addendums` T11 ON(T11.requested_procedure_id = T1.id) WHERE T1.id = '".$id."'";

            $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.approve_user_name, T1.approve_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T2.institution_id, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T7.mpps, T7.cdmc, T7.administrative_id as ci, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer, T11.text FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN `mediris`.`sexes` T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN `mediris`.`patients` T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.approve_user_id) LEFT JOIN `mediris`.`prefixes` T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN `mediris`.`institutions` T10 ON(T10.id = T2.institution_id) LEFT JOIN `apimeditron`.`addendums` T11 ON(T11.requested_procedure_id = T1.id) WHERE T1.id = '".$id."'";
        }

        $info = DB::select($sql);

        $texto = html_entity_decode($info[0]->text);

        $texto = preg_replace("/background-color[\s]*:[\s]*#[\w]+;?/i", "", $texto); //background-color:#ffffff;
        $texto = preg_replace("/box-sizing[\s]*:[\s]*[\w-]+[;]?/i", "", $texto);     //box-sizing : border-box;
        $texto = preg_replace("/text-autospace[\s]*:[\s]*[\w]+[;]?/i", "", $texto);  //text-autospace : none;
        $texto = preg_replace("/<img([\w\W]+?)\/>/i", "", $texto);

        $info[0]->signature = str_replace("users/", "firmas/", $info[0]->signature);

        // $data = ['texto' => $texto, 'nombres' => $info[0]->patient_first_name, 'apellidos' => $info[0]->patient_last_name, 'cedula' => $info[0]->patient_identification_id, 'sexo' => $this->obtenerSexo($info[0]->sexo), 'fecha_nacimiento' => date('d-m-Y', strtotime($info[0]->birth_date)), 'accession_number' => $info[0]->orden, 'descripcion' => $info[0]->procedimiento, 'fecha_creacion' =>  strftime("%B %d, %Y", strtotime(date($info[0]->issue_date))), 'firma' => $url . "" . $info[0]->signature, 'doctor' => $info[0]->prexif ." ". $info[0]->radiologist_user_name, 'patient_type' => $info[0]->patient_type, 'referido' => $this->obtenerReferido($info[0]->nombre_referido, $info[0]->apellido_referido), 'especialidad' => $info[0]->position, 'edad' => $this->calculaEdad(date('Y-m-d', strtotime($info[0]->birth_date))), 'fecha_dictado' => strftime("%B %d, %Y", strtotime(date($info[0]->culmination_date))), 'header_img' => $url . "" . $info[0]->report_header, 'footer_img' => $url . "" . $info[0]->report_footer ];

        $data = ['texto' => $texto, 'nombres' => $info[0]->patient_first_name, 'apellidos' => $info[0]->patient_last_name, 'cedula' => $info[0]->patient_identification_id, 'sexo' => $this->obtenerSexo($info[0]->sexo), 'fecha_nacimiento' => date('d-m-Y', strtotime($info[0]->birth_date)), 'accession_number' => $info[0]->orden, 'descripcion' => $info[0]->procedimiento, 'fecha_creacion' =>  strftime("%B %d, %Y", strtotime(date($info[0]->issue_date))), 'firma' => $url . "" . $info[0]->signature, 'doctor' => $info[0]->prexif ." ". $info[0]->approve_user_name, 'patient_type' => $info[0]->patient_type, 'referido' => $this->obtenerReferido($info[0]->nombre_referido, $info[0]->apellido_referido), 'especialidad' => $info[0]->position, 'edad' => $this->calculaEdad(date('Y-m-d', strtotime($info[0]->birth_date))), 'fecha_dictado' => strftime("%B %d, %Y", strtotime(date($info[0]->culmination_date))), 'header_img' => $url . "" . $info[0]->report_header, 'footer_img' => $url . "" . $info[0]->report_footer, 'mpps' => $info[0]->mpps, 'cdmc' => $info[0]->cdmc, 'ci' => $info[0]->ci];

        $pdf = PDF::loadView('reporte', $data);

        if($t == 'O'){
            $nombre = "Informe Orden N° ".$info[0]->orden.'.pdf';
        }else{
            $nombre = "Addendum Orden N° ".$info[0]->orden.'.pdf';
        }

        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed'=> TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );

        return $pdf->download($nombre);
    }


    public function sendReport(Request $request){

        $data = $request->all();

        if($data['opcion'] == 1){

            $url = $this->getDataInstitution('url_external_api')."/correo/".$this->getAdministrativeId($data['orden'])."-" . $data['orden'];

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = json_decode(curl_exec($ch), true);
            curl_close($ch);

            $respuesta = json_decode($result, true);

            if($respuesta['documents'][0]['nModified'] == 1 || $respuesta['documents'][0]['nModified'] == 0){
                $response = array("status" => '200', "mensaje" => 'Se envió el informe correctamente', "data" => $respuesta);
            }else{
                $response = array("status" => '500', "mensaje" => 'No se envió el informe', "data" => $respuesta);
            }

            return $response;

        }else{

            $tipo = '';
            $addendum = $this->validarAddendums($data['orden']);

            if($addendum){
                $tipo = 'A';
            }else{
                $tipo = 'O';
            }

            $url = $this->getDataInstitution('url_external_api')."/reporte/".$this->getAdministrativeId($data['orden'])."-" . $data['orden'] . "-". $tipo;

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = json_decode(curl_exec($ch), true);
            curl_close($ch);

            $respuesta = json_decode($result, true);

            if($respuesta['documents'][0]['accession_number'] == null && $respuesta['documents'][0]['processed']){
                $response = array("status" => '500', "mensaje" => 'No se envió el informe', "data" => '');
            }else{
                $response = array("status" => '200', "mensaje" => 'Se envió el informe correctamente', "data" => array("id" => $respuesta['documents'][0]['_id'], "accession_number" =>  $respuesta['documents'][0]['accession_number'], "institution" => $respuesta['documents'][0]['institution']));
            }

            return $response;
        }
    }
    

    public function calculaEdad($fechaNacimiento){
        list($ano, $mes, $dia) = explode("-", $fechaNacimiento);
        
        $anoDiferencia  = date("Y") - $ano;
        $mesDiferencia = date("m") - $mes;
        $diaDiferencia   = date("d") - $dia;
        
        if ($diaDiferencia < 0 || $mesDiferencia < 0){
            $anoDiferencia--;
        }
        
        return $anoDiferencia;
    }


    public function obtenerSexo($sexo){

        if($sexo == 'F'){
            $descripcion = "Femenino";
        }else{
            $descripcion = "Masculino";
        }

        return $descripcion;
    }


    public function obtenerReferido($nombre, $apellido){

        if($nombre && $apellido){
            $referido = $nombre ." ". $apellido;
        }else{
            $referido = "No Especificado";
        }

        return $referido;
    }

    public function validarAddendums($orden){

        $sql = "SELECT * FROM `apimeditron`.`addendums` WHERE requested_procedure_id = '".$orden."'";
        $addendum = DB::select($sql);

        return count($addendum) > 0 ? true : false;
    }


    public function validarInforme($orden){

        $sql = "SELECT * FROM `apimeditron`.`requested_procedures` WHERE id = '".$orden."'";
        $informe = DB::select($sql);

        return $informe[0]->text != '' ? true : false; 
    }


    public function reportViewmed(Request $request){

        $start_date = $request->all()['start_date'];
        $end_date = $request->all()['end_date'];
        
        $viewmedOrdenes = $this->ordenesViewmed($start_date, $end_date);

        if(count($viewmedOrdenes) > 0){

            $medirisOrdenes = $this->ordenesMediris($start_date, $end_date);

            foreach($medirisOrdenes as $key => $value){
    
                $value->status = $this->convertStatus($value->status);

                if($value->culmination_date != '0000-00-00 00:00:00'){
                    $value->culmination_date = date("d/m/Y", strtotime($value->culmination_date));
                }

                if($value->created_at != '0000-00-00 00:00:00'){
                    $value->created_date = $value->created_at;
                    $value->created_at = date("d/m/Y", strtotime($value->created_at));
                }

                $orden = $value->id;

                $value->viewmed = false;
                $value->cedula = 'N';
                $value->report = 'NO';
                $value->img_viewmed = 0;

                $institucion = $this->getDataInstitution('administrative_id');
                $facturacion = $this->getDataConf($institucion, 'facturacion');

                if(json_encode($facturacion) == 'true'){
                    $info_factura = $this->validarInfoFactura($orden);
                    $value->nro_factura = $info_factura['nro_factura'];
                    $value->estatus_factura = $info_factura['estatus_factura'];
                }

                $value->img_pacs = $this->validarImagenPACS($orden);

                foreach($viewmedOrdenes as $a => $b){

                    if($b['accession_number'] == $orden){
                        if($value->patient_identification_id == $b['patient_id']){
                            $value->cedula = 'S';
                        }else{
                            $value->cedula = 'E';
                        }
                        $value->cedulaViewmed = $b['patient_id'];
                        $value->report = $b['report'] == true ? 'SI' : 'NO';
                        $value->viewmed = true;
                        $value->img_viewmed = $b['number_imagings'];
                        unset($viewmedOrdenes[$a]);
                        break;
                    }
                }
            }

            $report = $this->generarReporte($medirisOrdenes, $start_date, $end_date, 'facturacion');

            return $report;
        }else{
            return redirect()->route('home')->with('warning','No hay ordenes en viewmed en el rango de fecha seleccionado');
        }
    }


    public function ordenesViewmed($start_date, $end_date){

        $institucion = $this->getDataInstitution('administrative_id');

        $data = array("institution" => $this->getDataConf($institucion, 'id_institution_viewmed'), 'from' => $start_date, "to" => $end_date);

        $url = "https://api.viewmedonline.com:8443/studies/agg/uploaded";

        // Se encodea el JSON para enviarlo 
        $jsonDataEncoded = json_encode($data);

        // Definimos la url a la cual se realizara la peticion
        $ch = curl_init($url);

        // Indicamos que nuestra petición sera Post
        curl_setopt($ch, CURLOPT_POST, 1);

        // Indicamos que nuestra petición sera Get pero podremos enviar un body
        //curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

        // Para que la peticion retorne un resultado y podamos manipularlo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Adjuntamos el json a nuestra petición
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        // Agregamos los encabezados del contenido
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         
        // Ejecutamos la petición y almacenamos la respuesta en la variable $result
        $result = curl_exec($ch);

        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);

        return json_decode($result, true)['documents'];
    }


    public function ordenesMediris($start_date, $end_date){

        $start_date = $start_date . " 00:00:00";
        $end_date = $end_date . " 23:59:59";

        $sql = "SELECT T1.id, T1.service_request_id, T1.procedure_id, T1.requested_procedure_status_id, T1.culmination_date, T1.created_at, T1.suspension_reason_id, T2.patient_id, T2.patient_first_name, T2.patient_last_name, T2.patient_identification_id, T2.user_id, T7.telephone_number, T7.cellphone_number, T7.email, T3.description, T3.modality_id, T4.description AS status, T5.name AS modalidad, T6.description as suspension, T1.technician_user_name, T1.technician_end_date, T1.radiologist_user_name, T1.dictation_date, T1.transcriptor_user_name, T1.transcription_date, T1.approve_user_name, T1.approval_date, T1.culmination_date , T1.reject_user_name, T1.reject_date, T1.suspension_user_name, T1.suspension_date, T8.first_name, T8.last_name FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN `apimeditron`.`procedures` T3 ON(T3.id = T1.procedure_id) LEFT JOIN `apimeditron`.`requested_procedure_statuses` T4 ON(T4.id = T1.requested_procedure_status_id) LEFT JOIN `apimeditron`.`modalities` T5 ON(T5.id = T3.modality_id) LEFT JOIN `apimeditron`.`suspend_reasons` T6 ON(T6.id = T1.suspension_reason_id) INNER JOIN `mediris`.`patients` T7 ON(T7.id = T2.patient_id) INNER JOIN `mediris`.`users` T8 ON(T8.id = T2.user_id) WHERE T1.created_at BETWEEN '".$start_date."' AND '".$end_date."'";

        $data = DB::select($sql);

        return $data;
    }

    public function convertStatus($status){

        $estatus = array('to-admit' => 'Por admitir',  'to-do' => 'Por realizar', 'to-dictate' => 'Por dictar', 'to-transcribe' => 'Por transcribir', 'to-approve' => 'Por aprobar', 'finished' => 'Finalizado', 'suspended' => 'Suspendido');

        return $estatus[$status];
    }


    public function generarReporte($data, $start_date, $end_date, $view, $ordenes_usuario = ""){

        $start_date = date("d/m/Y", strtotime($start_date));
        $end_date = date("d/m/Y", strtotime($end_date));
        $institucion = $this->getDataInstitution('administrative_id');

        if($view == 'resultados'){
            return (new ResultsExport($data, $start_date, $end_date, $ordenes_usuario))->download("Reporte resultados entregados por usuarios ".$this->getDataConf($institucion, 'name').".xlsx");
        }else if($view == 'auditoria'){
            return (new CorreosExport($data, $start_date, $end_date, $ordenes_usuario))->download("Reporte auditoria ".$this->getDataConf($institucion, 'name').".xlsx");
        }else if($view == 'referrings'){
            return (new ReferringsExport($data, $start_date, $end_date, $ordenes_usuario))->download("Reporte Referentes ".$this->getDataConf($institucion, 'name').".xlsx");
        }else{
            $facturacion = $this->getDataConf($institucion, 'facturacion');
            $fact = json_encode($facturacion) == 'true' ? 1 : 0;
            return (new ViewmedExport($data, $start_date, $end_date, $fact))->download("Reporte ".$this->getDataConf($institucion, 'name').".xlsx");
        }

    }


    public function changeID(Request $request){

        $data = $request->all();

        $url = $this->getDataInstitution('url_external_api')."/cedula/".$this->getAdministrativeId($data['orden'])."-" . $data['orden'] . "-" . $data['cedulanueva'] . "-" . $data['cedulavieja'];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $result;
    }


    public function reporteAuditoria(Request $request){

        $start_date = $request->all()['start_date1'] . " 00:00:00";
        $end_date = $request->all()['end_date1'] . " 23:59:59";

        $sql = "SELECT T1.id, T1.patient_id, T2.patient_ID, T1.patient_first_name, T1.patient_last_name, T2.telephone_number, T2.cellphone_number, T2.email, T1.user_id, T3.first_name, T3.last_name, T4.id as orden, T4.requested_procedure_status_id, T4.technician_user_name, T4.technician_end_date, T4.radiologist_user_name, T4.dictation_date, T4.transcriptor_user_name, T4.transcription_date, T4.approve_user_name, T4.approval_date, T4.culmination_date , T4.reject_user_name, T4.reject_date, T4.suspension_user_name, T4.suspension_date, T5.description FROM `apimeditron`.`service_requests` T1 INNER JOIN `mediris`.`patients` T2 ON(T2.id = T1.patient_id) INNER JOIN `mediris`.`users` T3 ON(T3.id = T1.user_id) INNER JOIN `apimeditron`.`requested_procedures` T4 ON(T4.service_request_id = T1.id) INNER JOIN `apimeditron`.`requested_procedure_statuses` T5 ON(T5.id = T4.requested_procedure_status_id) WHERE T1.issue_date BETWEEN '".$start_date."' AND '".$end_date."'";

        $data = DB::select($sql);

        $users = [];
        $ordenes_usuario = [];

        foreach($data as $a){

            $a->technician_end_date = $a->technician_end_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->technician_end_date)) : '';

            $a->dictation_date = $a->dictation_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->dictation_date)) : '';
            
            $a->transcription_date = $a->transcription_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->transcription_date)) : '';
            
            $a->approval_date = $a->approval_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->approval_date)) : '';
            
            $a->culmination_date = $a->culmination_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->culmination_date)) : '';
            
            $a->reject_date = $a->reject_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->reject_date)) : '';
            
            $a->suspension_date = $a->suspension_date != '0000-00-00 00:00:00' ? date("d/m/Y", strtotime($a->suspension_date)) : '';

            $a->description = $this->convertStatus($a->description);

            if(!in_array($a->user_id, $users)){
                array_push($users, $a->user_id);
            }
        }

        foreach($users as $user){
            $cont = 0;
            $usuario = "";
            foreach($data as $b){
                if($b->user_id == $user){
                    $cont++;
                    $usuario = $b->first_name .  " " . $b->last_name;
                }
            }

            array_push($ordenes_usuario, array("id" => $user, "usuario" => $usuario, "ordenes" => $cont));
        }

        $report = $this->generarReporte($data, $start_date, $end_date, 'auditoria', $ordenes_usuario);

        return $report;
    }


    public function findDelivers(Request $request){

        $start_date = $request->all()['start_date2'] . " 00:00:00";
        $end_date = $request->all()['end_date2'] . " 23:59:59";

        $sql = "SELECT * FROM `apimeditron`.`delivers` WHERE date BETWEEN '".$start_date."' AND '".$end_date."'";

        $data = DB::select($sql);

        $users = [];
        $ordenes_usuario = [];

        foreach($data as $a){
            if(!in_array($a->responsable_id, $users)){
                array_push($users, $a->responsable_id);
            }
        }

        foreach($users as $user){
            $cont = 0;
            $usuario = "";
            foreach($data as $b){
                if($b->responsable_id == $user){
                    $cont++;
                    $usuario = $b->responsable_name;
                }
            }

            array_push($ordenes_usuario, array("id" => $user, "usuario" => $usuario, "ordenes" => $cont));
        }

        if(count($ordenes_usuario)>0){

            $start_date = date("d/m/Y", strtotime($start_date));
            $end_date = date("d/m/Y", strtotime($end_date));

            return $this->generarReporte($data, $start_date, $end_date, 'resultados', $ordenes_usuario);

        }else{
            return redirect()->route('home')->with('warning','No hay resultados entregados en el rango de fecha seleccionado');
        }

    }


    public function validarInfoFactura($orden){

        $url = $this->getDataInstitution('url_external_api')."/validarFacturacion/".$this->getAdministrativeId($orden)."-" . $orden;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $result;
    }


    public function validarImagenPACS($orden){

        $url = $this->getDataInstitution('url_external_api')."/validarimgs/".$this->getAdministrativeId($orden)."-" . $orden;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $result['busqueda'];
    }

    public function fillSelect($table){

        $sql = "SELECT id, first_name, last_name FROM `apimeditron`.`".$table."` WHERE active = '1'";
        $data = DB::select($sql);

        $data = json_encode($data);

        return json_decode($data, true);
    }

    public function findOrdersReferringsMedicalVisitors(Request $request){

        $data = $this->sqlReferringsMedicalVisitors($request);

        if(count($data) > 0){
            $result = array('aaData'=>$data);
        }else{
            $result = array('aaData'=>[]);
        }

        $data = json_encode($result);

        return $data;
    }

    public function exportReportReferrings(Request $request){

        $data = $this->sqlReferringsMedicalVisitors($request);

        $ordenes_usuario = '';

        $start_date = date("d/m/Y", strtotime($request->all()['start_date3'] . " 00:00:00"));
        $end_date = date("d/m/Y", strtotime($request->all()['end_date3'] . " 23:59:59"));

        $report = $this->generarReporte($data, $start_date, $end_date, 'referrings');

        return $report;
    }

    public function sqlReferringsMedicalVisitors($request){

        $datos = $request->all();

        $start_date = $datos['start_date3'] . " 00:00:00";
        $end_date = $datos['end_date3'] . " 23:59:59";

        $sql = "SELECT T1.id, CONCAT(T1.patient_first_name, ' ' , T1.patient_last_name) as patient, T1.patient_identification_id, T1.referring_id, T1.medical_visitor_id, T1.issue_date, T2.id AS orden, CONCAT(T3.last_name, ' ' ,T3.first_name) AS referring, T3.email AS email_referring, CONCAT(T4.last_name, ' ' ,T4.first_name) AS medical, T4.email AS email_medical FROM `apimeditron`.`service_requests` T1 JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id) JOIN `apimeditron`.`referrings` T3 ON(T3.id = T1.referring_id AND T3.active = '1') LEFT JOIN `apimeditron`.`medical_visitors` T4 ON(T4.id = T1.medical_visitor_id AND T4.active = '1') WHERE T1.issue_date BETWEEN '".$start_date."' AND '".$end_date."'";

        if(isset($datos['select_referrings'])){
            $sql.= " AND T1.referring_id = '".$datos['select_referrings']."'";
        }

        if(isset($datos['select_medical'])){
            $sql.= " AND T1.medical_visitor_id = '".$datos['select_medical']."'";
        }

        return DB::select($sql);
    }

    public function getDataInstitution($field){
        $sql = "SELECT * FROM `mediris`.`institutions` LIMIT 1";

        $institucion = DB::select($sql);
        $institucion = json_encode($institucion);

        return json_decode($institucion, true)[0][$field];
    }

    public function getDataConf($id, $field){
        return config('sedes.data.'.$id.'.'.$field);
    }

    public function getAdministrativeId($orden){
        $sql = "SELECT T3.administrative_id FROM `apimeditron`.`requested_procedures` T1 JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) JOIN `mediris`.`institutions` T3 ON(T3.id = T2.institution_id) WHERE T1.id = '".$orden."'";
        
        $id = DB::select($sql);
        $id = json_encode($id);
        return json_decode($id, true)[0]['administrative_id'];
    }
}
